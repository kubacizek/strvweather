//
//  TodayViewController.swift
//  weather-widget
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit
import NotificationCenter

class WidgetViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var forecastIcon: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let _ = LocationManagerInstance
        self.preferredContentSize = CGSize(width: 0, height: 60); // for iOS 9
        NSNotificationCenterDefault.addObserver(forName: .locationManagerDidUpdateLocation, object: nil, queue: nil) { notification in
            self.loadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        self.loadData()
        completionHandler(NCUpdateResult.newData)
    }
    
    func loadData() {
        API.getWeather() { data, error in
            guard let data = data,
                let temp = data.main?.temp,
                let desc = data.weather?.first,
                let weatherMain = desc.main
                else {
                    print("Error: \(error)")
                    return
            }
            
            let roundedTemp = Int(round(temp))
            
            self.descLabel.text = "\(roundedTemp)°C | \(weatherMain)"
            
            switch weatherMain {
            case "Rain":
                self.forecastIcon.image = UIImage(named: "CR")
            case "Thunderstorm":
                self.forecastIcon.image = UIImage(named: "forecast_CL")
            case "Drizzle":
                self.forecastIcon.image = UIImage(named: "forecast_Forecast")
            case "Snow":
                self.forecastIcon.image = UIImage(named: "forecast_Forecast")
            case "Clouds":
                self.forecastIcon.image = UIImage(named: "forecast_CS")
            case "Extreme":
                self.forecastIcon.image = UIImage(named: "forecast_Wind")
            default:
                self.forecastIcon.image = UIImage(named: "forecast_Sun")
            }
        }
    }
}
