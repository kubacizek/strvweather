//
//  WeatherResponseMain.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import ObjectMapper

class WeatherResponseMain: Mappable {
    var temp: Float?
    var pressure: Int?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        temp		<- map["temp"]
        pressure	<- map["pressure"]
    }
}

class WeatherResponseWind: Mappable {
    var speed: Float?
    var deg: Int?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        speed		<- map["speed"]
        deg			<- map["deg"]
    }
}

class WeatherResponseRain: Mappable {
    var volume: Float?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        volume		<- map["1h"]
    }
}

class WeatherResponseClouds: Mappable {
    var all: Int?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        all			<- map["all"]
    }
}

class WeatherResponseWeather: Mappable {
    var main: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        main		<- map["main"]
    }
}

class WeatherResponse: Mappable {
    var name: String?
    var main: WeatherResponseMain?
    var wind: WeatherResponseWind?
    var clouds: WeatherResponseClouds?
    var weather: [WeatherResponseWeather]?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name		<- map["name"]
        main		<- map["main"]
        wind		<- map["wind"]
        clouds		<- map["clouds"]
        weather		<- map["weather"]
    }
}
