//
//  AppDelegate.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var forecast: [ForecastResponseItem] = []
    let dateFormatter = DateFormatter()
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var offlineOverlay: UIView!
    @IBOutlet weak var offlineLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateFormatter.dateFormat = "EEEE"
		NSNotificationCenterDefault.addObserver(forName: .locationManagerDidUpdateLocation, object: nil, queue: nil) { notification in
			self.loadData()
		}
    }

    func loadData() {
		API.getForecast(days: 10) { data, error in
            if let data = data, let list = data.list {
				self.forecast = list
				self.tableView.reloadData()
			} else {
                self.offlineOverlay.alpha = 1
				print("Error: \(error)")
                return
			}
 
            UIView.animate(withDuration: 0.5, animations: {
                self.offlineOverlay.alpha = 0
            })
        }
    }
    
    @IBAction func retryAction(_ sender: Any) {
        if LocationManagerInstance.currentStatus == .authorizedAlways {
            loadData()
        }
    }
    
    func convertUnixTimeToDayName(_ unixTime: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval.init(unixTime))
        return self.dateFormatter.string(from: date)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ForecastTableViewCell
        
		let item = self.forecast[indexPath.row]
		
		guard let unixDate = item.dt,
			  let weather = item.weather?.first,
			  let temp = item.temp,
			  let max = temp.max,
			  let desc = weather.main
			  else {
			return cell
		}
		
        let roundedTemp = Int(round(max))

        cell.temperature.text = "\(roundedTemp)°"
        cell.desc.text = desc
        cell.day.text = self.convertUnixTimeToDayName(unixDate)
        
        switch desc {
            case "Rain":
                cell.forecastIcon.image = UIImage(named: "CR")
            case "Thunderstorm":
                cell.forecastIcon.image = UIImage(named: "forecast_CL")
            case "Drizzle":
                cell.forecastIcon.image = UIImage(named: "forecast_Forecast")
            case "Snow":
                cell.forecastIcon.image = UIImage(named: "forecast_Forecast")
            case "Clouds":
                cell.forecastIcon.image = UIImage(named: "Cloudy_Big")
            case "Extreme":
                cell.forecastIcon.image = UIImage(named: "WInd_Big")
            default:
                cell.forecastIcon.image = UIImage(named: "forecast_Sun")
        }
        
        return cell
    }
}
