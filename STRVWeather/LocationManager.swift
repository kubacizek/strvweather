//
//  LocationManager.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    internal var manager: CLLocationManager = CLLocationManager()
    internal var authstatus: CLAuthorizationStatus = .notDetermined
    
    fileprivate override init() {
        super.init()
        print("init LocationManager")
        self.manager.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.manager.requestAlwaysAuthorization()
    }
    
    var currentLocation: CLLocation? {
        return self.manager.location
    }
    
    var currentStatus: CLAuthorizationStatus? {
        return self.authstatus
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorizationStatus")
        authstatus = status
        
        switch status {
        case .notDetermined:
            print(".NotDetermined")
            break
            
        case .authorizedAlways:
            print(".Authorized")
            self.manager.startUpdatingLocation()
            break
            
        case .denied:
            print(".Denied")
            break
            
        default:
            print("Unhandled authorization status")
            break
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        NSNotificationCenterDefault.post(name: .locationManagerDidUpdateLocation, object: location)
    }
}

let LocationManagerInstance = LocationManager()
