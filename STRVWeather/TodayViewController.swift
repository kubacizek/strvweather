//
//  AppDelegate.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit

class TodayViewController: UIViewController {
    
    @IBOutlet weak var forecastIcon: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
	
    @IBOutlet weak var forecastLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var celsiusLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var compassLabel: UILabel!
    
    @IBOutlet weak var offlineOverlay: UIView!
    @IBOutlet weak var offlineLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    
    var dataLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
		NSNotificationCenterDefault.addObserver(forName: .locationManagerDidUpdateLocation, object: nil, queue: nil) { notification in
			self.loadData()
		}
    }
    
    func degToCompass(_ num: Double) -> AnyObject {
        let val = floor((num / 22.5) + 0.5)
        let arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        return arr[Int((val.truncatingRemainder(dividingBy: 16)))] as AnyObject
    }
    
    func loadData() {
        API.getWeather() { _data, error in
			guard let data = _data,
				  let name = data.name,
				  let temp = data.main?.temp,
				  let desc = data.weather?.first,
				  let weatherMain = desc.main,
				  let windSpeed = data.wind?.speed,
				  let pressure = data.main?.pressure,
				  let cloudiness = data.clouds?.all,
				  let windDirection = data.wind?.deg
				  else {
                    self.offlineOverlay.alpha = 1
                    print("Error: \(error) \(_data)")
                    return
			}
            
            UIView.animate(withDuration: 0.5, animations: {
                self.offlineOverlay.alpha = 0
            })
            
            guard let position = LocationManagerInstance.currentLocation else { return }
			
			let roundedTemp = Int(round(temp))
            
            self.cityLabel.text = name
            self.temperatureLabel.text = "\(roundedTemp)°C | \(weatherMain)"
            
            self.forecastLabel.text = "\(cloudiness) %"
            self.celsiusLabel.text = "\(pressure) hPa"
            self.windLabel.text = "\(windSpeed) km/h"
            self.compassLabel.text = "\(self.degToCompass(Double(windDirection)))"
            
            switch weatherMain {
                case "Rain":
                    self.forecastIcon.image = UIImage(named: "Cloudy_Big")
                case "Thunderstorm":
                    self.forecastIcon.image = UIImage(named: "Lightning_Big")
                case "Drizzle":
                    self.forecastIcon.image = UIImage(named: "Cloudy_Big")
                case "Snow":
                    self.forecastIcon.image = UIImage(named: "Cloudy_Big")
                case "Clouds":
                    self.forecastIcon.image = UIImage(named: "Cloudy_Big")
                case "Extreme":
                    self.forecastIcon.image = UIImage(named: "WInd_Big")
                default:
                    self.forecastIcon.image = UIImage(named: "forecast_Sun")
            }
            
            addDictionaryToFireBase(WeatherDatabaseObject.init(city: name,
                                                               longitude: position.coordinate.longitude,
                                                               latitude: position.coordinate.latitude,
                                                               temperature: temp))
            self.dataLoaded = true
        }
    }
    
    @IBAction func retryAction(_ sender: Any) {
        if LocationManagerInstance.currentStatus == .authorizedAlways {
            loadData()
        }
    }

    @IBAction func share(_ sender: AnyObject) {
        if dataLoaded == true {
            let so:URL = URL(string:"http://strv.com")!
            let activityVC = UIActivityViewController(activityItems: ["It's \(self.temperatureLabel.text!) in \(self.cityLabel.text!)", so], applicationActivities: nil)
            
            let button = sender as! UIButton
            activityVC.popoverPresentationController?.sourceView = button
            self.navigationController!.present(activityVC, animated: true, completion: nil)
        }
    }
}

