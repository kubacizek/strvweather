//
//  AppDelegate.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

	override func viewDidLoad() {
		super.viewDidLoad()
		
		guard let vcs = self.viewControllers else { return }
		vcs[0].tabBarItem = UITabBarItem(title: "Today", image: UIImage(named: "forecast_Today")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "Sun"))
		vcs[1].tabBarItem = UITabBarItem(title: "Forecast", image: UIImage(named: "Forecast")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "forecast_Forecast"))
	}
}
