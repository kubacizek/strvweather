//
//  WeatherDatabaseObject.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit
import Firebase

class WeatherDatabaseObject: NSObject {
    let city: String
    let longitude: Double
    let latitude: Double
    let temperature: Float
    
    init(city: String, longitude: Double, latitude: Double, temperature: Float) {
        self.city = city
        self.longitude = longitude
        self.latitude = latitude
        self.temperature = temperature
    }
}

struct WeatherDataItem {
    
    let key: String
    let city: String
    let lat: Double
    let long: Double
    let temp: Float
    let date: String
    let ref: FIRDatabaseReference?
    
    init(city: String, lat: Double, long: Double, temp: Float, key: String = "") {
        self.key = key
        self.city = city
        self.lat = lat
        self.long = long
        self.temp = temp
        self.date = Date().description
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        city = snapshotValue["city"] as! String
        lat = snapshotValue["lat"]  as! Double
        long = snapshotValue["long"] as! Double
        temp = snapshotValue["temp"] as! Float
        date = snapshotValue["date"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "city": city,
            "lat": lat,
            "long": long,
            "temp": temp,
            "date": date
        ]
    }
}
