//
//  Extensions.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit
import Firebase

extension UIColor {
    convenience init(intColor: UInt) {
        self.init(red: CGFloat((intColor>>16)&0xFF)/255.0, green: CGFloat((intColor>>8)&0xFF)/255.0, blue: CGFloat((intColor)&0xFF)/255.0, alpha: CGFloat((intColor>>24)&0xFF)/255.0)
    }
}

func addDictionaryToFireBase(_ weatherData: WeatherDatabaseObject) {
    
    let ref = FIRDatabase.database().reference(withPath: "location-data")
    let weatherItem = WeatherDataItem(city: weatherData.city,
                                      lat: weatherData.latitude,
                                      long: weatherData.longitude,
                                      temp: weatherData.temperature)
    
    let cityRef = ref.child(weatherData.city)
    let weatherItemRef = cityRef.child(NSDate().description)
    weatherItemRef.setValue(weatherItem.toAnyObject())
}
