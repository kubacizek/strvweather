//
//  ForecastResponse.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import ObjectMapper

class ForecastResponseTemp: Mappable {
    //	day = "-18.52";
    //	eve = "-18.52";
    var max: Float?
    //	min = "-20.15";
    //	morn = "-18.52";
    //	night = "-20.15";
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        max			<- map["max"]
    }
}

class ForecastResponseWeather: Mappable {
    //	description = "light snow";
    //	icon = 13d;
    //	id = 600;
    var main: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        main		<- map["main"]
    }
}

class ForecastResponseItem: Mappable {
    //	clouds = 56;
    //	deg = 25;
    var dt: Int?
    //	humidity = 71;
    //	pressure = "997.34";
    //	snow = "0.25";
    //	speed = "1.5";
    
    var temp: ForecastResponseTemp?
    var weather: [ForecastResponseWeather]?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        dt			<- map["dt"]
        temp		<- map["temp"]
        weather		<- map["weather"]
    }
}

class ForecastResponse: Mappable {
    var list: [ForecastResponseItem]?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        list		<- map["list"]
    }
}
