//
//  API.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class API {
    
    fileprivate static let BaseURL = "http://api.openweathermap.org/data/2.5"
    fileprivate static let ApiKey = "2617b316fd81ff5e0dfac8edca339656"
    
    static func getWeather(_ completion: ((_ data: WeatherResponse?, _ error: Error?) -> Void)?) {
        guard let position = LocationManagerInstance.currentLocation else { return }
        
        let parameters: [String: Any] = [
            "lat": position.coordinate.latitude,
            "lon": position.coordinate.longitude,
            "units": "metric",
            "appid": ApiKey
        ]
        
        Alamofire.request(BaseURL+"/weather", method: .get, parameters: parameters).responseObject { (response: DataResponse<WeatherResponse>) in
            completion?(response.result.value, response.result.error)
        }
    }
    
    static func getForecast(days: Int, completion: ((_ data: ForecastResponse?, _ error: Error?) -> Void)?) {
        guard let position = LocationManagerInstance.currentLocation else { return }
        
        let parameters: [String: Any] = [
            "lat": position.coordinate.latitude,
            "lon": position.coordinate.longitude,
            "units": "metric",
            "cnt": days,
            "appid": ApiKey
        ]
        
        Alamofire.request(BaseURL+"/forecast/daily", method: .get, parameters: parameters).responseObject { (response: DataResponse<ForecastResponse>) in
            completion?(response.result.value, response.result.error)
        }
    }
}
