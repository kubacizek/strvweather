//
//  Constants.swift
//  STRVWeather
//
//  Created by Kuba on 27.11.16.
//  Copyright © 2016 Kuba. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let locationManagerDidUpdateLocation = NSNotification.Name(rawValue: "LocationManagerDidUpdateLocation")
}

let NSNotificationCenterDefault = NotificationCenter.default
